package com.example.sip.repository;

import com.example.sip.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

    @Query("select b from Book b join b.author")
    List<Book> findAllBooks();

    @Query("select b from Book b where b.id =?1")
    Book findByIds(Long id);
}
