package com.example.sip.controller;

import com.example.sip.dto.BookDto;
import com.example.sip.model.Author;
import com.example.sip.model.Book;
import com.example.sip.repository.AuthorRepository;
import com.example.sip.repository.BookRepository;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.catalina.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/book")

public class BookController {

    @Autowired
    BookRepository bookRepository;
    @Autowired
    AuthorRepository authorRepository;

    @GetMapping("/all")
    public List<Book> all() {
        return bookRepository.findAllBooks();
    }

    @GetMapping("/{id}")
    public  Book find(@PathVariable(value = "id") Long id) {
        Book result = bookRepository.findByIds(id);
        return result;
    }

    @PostMapping("")
    public Book create(BookDto bookDto) {
        Author author = authorRepository.findById(bookDto.getAuthor_id()).orElse(null);
        Book book = new Book();
//        return newBook;
    }

//    @PutMapping("/{id}")
//    public Book update(@PathVariable(value = "id") Long bukuId, @Valid @RequestBody Book book) {
//        return null;
//    }

//    @DeleteMapping("/buku/{id}")
//    public Buku delete(@PathVariable(name = "id") Long bukuId) {
//        Buku buku = bukuRepository.findByIds(bukuId);
//     return bukuRepository.delete(buku);
//    }
}
