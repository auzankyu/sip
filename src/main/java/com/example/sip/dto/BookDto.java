package com.example.sip.dto;

import com.example.sip.model.Book;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Setter @Getter
public class BookDto {

    private String category;
    private String title;
    private Date year;
    private Long author_id;

    public void setAuthor_id(Long author_id) {
        this.author_id = author_id;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setYear(Date year) {
        this.year = year;
    }

    public Date getYear() {
        return year;
    }

    public Long getAuthor_id() {
        return author_id;
    }

    public String getCategory() {
        return category;
    }

    public String getTitle() {
        return title;
    }
}
